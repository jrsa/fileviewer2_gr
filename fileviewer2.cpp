#include <gnuradio/top_block.h>
#include <gnuradio/sync_block.h>
#include <gnuradio/io_signature.h>

#include <gnuradio/blocks/add_const_ff.h>
#include <gnuradio/blocks/char_to_float.h>
#include <gnuradio/blocks/complex_to_mag.h>
#include <gnuradio/blocks/file_source.h>
#include <gnuradio/blocks/file_sink.h>
#include <gnuradio/blocks/null_sink.h>
#include <gnuradio/blocks/throttle.h>
#include <gnuradio/blocks/tag_debug.h>
#include <gnuradio/digital/binary_slicer_fb.h>

#include <QWidget>
#include <gnuradio/qtgui/time_sink_f.h>

#include <iostream>
#include <thread>
#include <chrono>

using namespace gr;
using namespace std::chrono_literals;


class pwm_decode : public gr::sync_block {
public:
    typedef boost::shared_ptr<pwm_decode> sptr;

    static sptr make()
    {
      return gnuradio::get_initial_sptr
        (new pwm_decode());
    }

    int work (int noutput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
    constexpr int port { 0 };
      auto items = input_items[port];
      auto out = output_items[port];
      for (int i = 0; i < noutput_items; i++) {
        uint64_t index = nitems_read(port) + i;
        float item = ((float *) items)[i];
        ((float *) out)[i] = item;
        // edge
        if (item != last) {
          // TODO: make this a configurable option?
          // add_item_tag(port, index, pmt::intern("edge"), pmt::from_float(item));
          if (item == 0) {
            add_item_tag(port, index, pmt::intern("pulse_length"), pmt::from_float(index - last_edge_index_));
          }
          last_edge_index_ = index;
        }
        last = item;
      }

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }
private:

    float last = 0;
    uint64_t last_edge_index_ = 0;

    pwm_decode() : gr::sync_block(
              "pwm_decode",
              gr::io_signature::make(1, 1, sizeof(float)),
              gr::io_signature::make(1, 1, sizeof(float)))
    {}

};

// XXX not functional yet, just counts the tags
class pulse_length : public gr::sync_block {
public:
  typedef boost::shared_ptr<pulse_length> sptr;

  static sptr make()
  {
    return gnuradio::get_initial_sptr
      (new pulse_length());
  }

  int work (int noutput_items, gr_vector_const_void_star &input_items, gr_vector_void_star &output_items)
  {
    constexpr int port { 0 };
    auto items = input_items[port];
    auto out = output_items[port];

    // check for edge tags
    get_tags_in_window(cur_work_tags_, port, 0, noutput_items, pmt::intern("pulse_length"));

    for ( auto tag : cur_work_tags_ ) {
      std::cout << tag.value << ", ";
    }
    std::cout << std::endl;

    // copy items through, although we don't really need the raw samples anymore... hmm...
    for (int i = 0; i < noutput_items; i++) {
      float item = ((float *) items)[i];
      ((float *) out)[i] = item;
    }

    // Tell runtime system how many output items we produced.
    return noutput_items;
  }

  size_t num_work_tags()
  {
    return cur_work_tags_.size();
  }

private:
  std::vector<tag_t> cur_work_tags_;

  pulse_length() : gr::sync_block(
            "pulse_length",
            gr::io_signature::make(1, 1, sizeof(float)),
            gr::io_signature::make(1, 1, sizeof(float)))
  {}
};

int main (int argc, char *argv[])
{
    if (argc < 2)
      return -1;

    constexpr char* file_start_tag_ { "filestart" };
    constexpr int samp_rate { (int) 2e6 };

    top_block_sptr top_block = gr::make_top_block("acuriteTest");

    digital::binary_slicer_fb::sptr   digital_binary_slicer_fb_0 { digital::binary_slicer_fb::make() };

    blocks::null_sink::sptr      null_sink_0      { blocks::null_sink::make(sizeof(float)) };
    blocks::complex_to_mag::sptr complex_to_mag_0 { blocks::complex_to_mag::make(1) };
    blocks::char_to_float::sptr  char_to_float_0  { blocks::char_to_float::make(1, 1) };
    blocks::add_const_ff::sptr   add_const_vxx_0  { blocks::add_const_ff::make(-0.04) };
    blocks::file_source::sptr    file_source_0    { blocks::file_source::make(sizeof(gr_complex), argv[1], true, 0, 0) };
    blocks::throttle::sptr       throttle_0       { blocks::throttle::make(sizeof(gr_complex), samp_rate, true) };
    blocks::tag_debug::sptr       tag_debug       { blocks::tag_debug::make(sizeof(float), "tag debug hi") };

    qtgui::time_sink_f::sptr scope { qtgui::time_sink_f::make(sizeof(float), samp_rate, "i hope this works") };

    // scope settings
    scope->set_nsamps(1e6);

    file_source_0->set_begin_tag(pmt::intern(file_start_tag_));
    scope->set_trigger_mode(qtgui::TRIG_MODE_TAG, qtgui::TRIG_SLOPE_POS, 0, 0, 0, file_start_tag_);

    top_block->hier_block2::connect(file_source_0, 0, throttle_0, 0);
    top_block->hier_block2::connect(throttle_0, 0, complex_to_mag_0, 0);
    top_block->hier_block2::connect(complex_to_mag_0, 0, add_const_vxx_0, 0);
    top_block->hier_block2::connect(add_const_vxx_0, 0, digital_binary_slicer_fb_0, 0);
    top_block->hier_block2::connect(digital_binary_slicer_fb_0, 0, char_to_float_0, 0);

    pwm_decode::sptr pwm_test { pwm_decode::make() };
    pulse_length::sptr pwm_counter { pulse_length::make() };

    top_block->hier_block2::connect(char_to_float_0, 0, pwm_test, 0);
    top_block->hier_block2::connect(pwm_test, 0, scope, 0);
    top_block->hier_block2::connect(pwm_test, 0, pwm_counter, 0);
    top_block->hier_block2::connect(pwm_counter, 0, null_sink_0, 0);
    // top_block->hier_block2::connect(pwm_test, 0, tag_debug, 0);

    top_block->start();

    // qapp
    QApplication app(argc, argv);
    scope->qwidget()->show();

    while (true) {
      // std::cout << tag_debug->nitems_read(0) << std::endl;
      std::cout << pwm_counter->num_work_tags() << std::endl;
      std::this_thread::sleep_for (0.01s);
      app.processEvents();
    }

    return 0;
}
